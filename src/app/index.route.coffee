angular.module 'tich'
  .config ($stateProvider, $urlRouterProvider, $locationProvider) ->
    'ngInject'
    # $stateProvider
    #   .state 'session',
    #     templateUrl: "app/views/templates/session.html"
    #     abstract: true
    $stateProvider
      .state 'home',
        abstract: true
        templateUrl: "app/views/templates/home.html"
        controller: "HomeController"
    $stateProvider
      .state 'home.login',
        url: "/ingresar"
        templateUrl: "app/views/session/login.html"
        controller:"LoginController"
    $stateProvider
      .state 'home.register',
        url: "/registrarse"
        templateUrl: "app/views/session/register.html"
        controller:"RegisterController"
    $stateProvider
      .state 'home.user_email',
        url: "/email"
        templateUrl: "app/views/session/email.html"
        controller:"EmailController"
    $stateProvider
      .state 'home.password_recovery',
        url: "/recuperar_password"
        templateUrl: "app/views/session/password_recovery.html"
        controller:"PasswordRecoveryController"
    $stateProvider
      .state 'home.confirmation',
        url: "/confirmacion"
        templateUrl: "app/views/session/confirmation.html"
    $stateProvider
      .state 'home.articles',
        url: "/"
        templateUrl: "app/views/articles/articles.html"
        controller:"ArticlesController"
    .state 'home.article',
        url: "/articulo/:slug"
        templateUrl: "app/views/articles/article.html"
        controller:"ArticleController"
    $stateProvider
      .state 'home.me',
        abstract:true
        templateUrl: "app/views/templates/me.html"
        resolve:
          auth: ($auth, $state) ->
            auth = $auth.validateUser()
            auth.then ->
              false
            .catch (data)->
              $state.go 'home.landing'
              false
            return auth
    $stateProvider
      .state 'home.me.profile',
        url: "/perfil"
        templateUrl: "app/views/me/profile.html"
        controller:"ProfileController"
    $stateProvider
      .state 'home.me.dashboard',
        url: "/dashboard"
        templateUrl: "app/views/me/dashboard.html"
        controller:"DashboardController"
    $stateProvider
      .state 'admin',
        abstract:true
        templateUrl: "app/views/admin/main.html"
        controller: "AdminController"
    $stateProvider
      .state 'admin.site',
        abstract:true
        url: "/admin"
        templateUrl: "app/views/admin/admin_site.html"
        controller:"AdminSiteController"
        resolve:
          auth: ($auth, $state) ->
            auth = $auth.validateUser()
            auth.then ->
              false
            .catch (data)->
              $state.go 'home.landing'
              false
            return auth
    $stateProvider
      .state 'admin.site.dashboard',
        url: "/dashboard"
        templateUrl: "app/views/admin/dashboard.html"
        controller:"DashboardAdminController"
      .state 'admin.site.categories',
        url: "/categorias"
        templateUrl: "app/views/admin/categories.html"
        controller:"CategoriesAdminController"
      .state 'admin.site.articles',
        url: "/articulos"
        templateUrl: "app/views/admin/articles.html"
        controller:"ArticlesAdminController"
      .state 'admin.site.article',
        url: "/articulo/:slug"
        templateUrl: "app/views/admin/article/article.html"
        controller:"AdminArticleController"
      .state 'admin.site.article.general',
        url: "/general"
        templateUrl: "app/views/admin/article/general.html"
        controller:"AdminArticleGeneralController"
      .state 'admin.site.article.content',
        url: "/contenido"
        templateUrl: "app/views/admin/article/content.html"
        controller:"AdminArticleContentController"
      .state 'admin.site.article.image',
        url: "/imagen"
        templateUrl: "app/views/admin/article/image.html"
        controller:"AdminArticleImageController"

    $urlRouterProvider.otherwise '/'
    $locationProvider.html5Mode(true)
