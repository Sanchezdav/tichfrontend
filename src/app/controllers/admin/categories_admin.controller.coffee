angular.module 'tich'
  .controller 'CategoriesAdminController', ($scope, $rootScope, Restangular, Pagination, $mdDialog) ->
    'ngInject'
    $rootScope.section =
      title:"Categorías"
    getPagination = (items) ->
      items.pagination = Pagination.getNew(10)
      items.pagination.numPages = Math.ceil(items.length/items.pagination.perPage)
    Restangular.all('admin').all('categories').getList().then (response) ->
      $scope.categories = response
      if $scope.categories
        getPagination($scope.categories)
    $scope.showSubcategories = (ev, category) ->
      Restangular.all('admin').all('subcategories').getList({category_id: category.id}).then (response) ->
        $scope.subcategories = response
      $mdDialog.show
        targetEvent: ev,
        locals: {parent: $scope},
        controller: angular.noop,
        controllerAs: 'ctrl',
        bindToController: true,
        clickOutsideToClose: true,
        templateUrl: 'app/views/dialogs/subcategories_admin_dialog.html'
