angular.module 'tich'
  .controller 'AdminArticleImageController', ($scope, $state, Upload, ngNotify, apiUrl) ->
    'ngInject'
    $scope.url = apiUrl
    $scope.upload = (file)->
      $scope.loading = true
      Upload.upload
        url: $scope.article.getRequestedUrl()
        file: file
        method: 'PUT'
      .success (data, status, headers, config)->
        $scope.article.image.image.preview.url = data.image.image.preview.url
        $scope.article.image.image.url = data.image.image.url
        $scope.loading = false
        ngNotify.set('¡Se guardó correctamente!', 'success')
        $scope.my_image = null
      .error (data, status, headers, config) ->
        $scope.loading = false
        ngNotify.set('¡Ocurrió un error!', 'error')
    $scope.uploadCancel = ->
      $scope.my_image = null