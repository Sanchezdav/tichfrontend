angular.module 'tich'
  .controller 'AdminArticleGeneralController', ($scope, $state, $timeout, Restangular) ->
    'ngInject'
    Restangular.all('categories').getList().then (response)->
      $scope.categories = response
    $scope.getSubcategories = ->
      if !$scope.article.category.slug?
        $scope.subcategories = []
        return
      Restangular.all('subcategories').getList({category: $scope.article.category.slug}).then (response)->
        $scope.subcategories = response