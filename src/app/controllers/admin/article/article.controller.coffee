angular.module 'tich'
  .controller 'AdminArticleController', ($scope, $state, $rootScope, Restangular, $stateParams, ngNotify, $mdDialog, wizMarkdownSvc) ->
    'ngInject'
    $rootScope.section =
      title: "Administra el artículo"
    Restangular.all('admin').one('articles', $stateParams.slug).get().then (response) ->
      $scope.article = response
      $scope.title_name = $scope.article.title
      $rootScope.$broadcast('articleGetted')
    $scope.updateArticle = ->
      $scope.loading = true
      if $scope.article.content_markdown != null
        textTransformed = wizMarkdownSvc.Transform($scope.article.content_markdown)
        $scope.article.content = textTransformed
      $scope.article.put().then (response) ->
        $scope.loading = false
        if response.title != $scope.title_name
          $state.go "admin.site.article.general", slug: response.slug
        ngNotify.set('¡Se guardó correctamente!', 'success')
      .catch (response) ->
        $scope.loading = false
        ngNotify.set('¡No se pudo guardar, intente de nuevo!', 'error')
    $scope.deleteArticleConfirm = (ev)->
      confirm = $mdDialog.confirm()
        .title('¿Estás seguro que deseas eliminar el artículo?')
        .content('Perderás toda la información')
        .ariaLabel('¿Estás seguro que deseas eliminar el artículo?')
        .targetEvent(ev)
        .ok('Aceptar')
        .cancel('cancelar')
      $mdDialog.show(confirm).then(->
        $scope.deleteArticle()
      , ->
        )
      false
    $scope.deleteArticle = ->
      $scope.article.remove().then ->
        $state.go 'admin.site.articles'
    $scope.publishArticle = ->
      if $scope.article.published == false
        $scope.article.published = true
        $scope.article.put().then (response) ->
          ngNotify.set('¡Se publicó correctamente!', 'success')
        .catch (response) ->
          ngNotify.set('¡No se pudo publicar, intente de nuevo!', 'error')
      else
        $scope.article.published = false
        $scope.article.put().then (response) ->
          ngNotify.set('¡Se eliminó del público correctamente!', 'success')
        .catch (response) ->
          ngNotify.set('¡No se pudo eliminar del público, intente de nuevo!', 'error')
