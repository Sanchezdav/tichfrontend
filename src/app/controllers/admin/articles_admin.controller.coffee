angular.module 'tich'
  .controller 'ArticlesAdminController', ($scope, $rootScope, Restangular, Pagination, $state, $mdDialog, apiUrl) ->
    'ngInject'
    $rootScope.section =
      title:"Artículos"
    $scope.url = apiUrl
    getPagination = (items) ->
      items.pagination = Pagination.getNew(10)
      items.pagination.numPages = Math.ceil(items.length/items.pagination.perPage)
    Restangular.all('admin').all('articles').getList().then (response) ->
      $scope.articles = response
      if $scope.articles
        getPagination($scope.articles)
    $scope.newArticle = (ev) ->
      $mdDialog.show
        targetEvent: ev,
        controller: 'NewArticleController',
        templateUrl: 'app/views/dialogs/new_article_dialog.html',
        clickOutsideToClose: true,
        scope: $scope.$new()