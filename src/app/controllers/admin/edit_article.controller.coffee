angular.module 'tich'
  .controller 'EditArticleController', ($scope, $rootScope, $state, $stateParams, Restangular, ngNotify, wizMarkdownSvc) ->
    'ngInject'
    $scope.subcategories = []
    $rootScope.section =
      title:"Edita el artículo"
    Restangular.all('admin').one('articles', $stateParams.slug).get().then (response) ->
      $scope.article = response
    Restangular.all('categories').getList().then (response)->
      $scope.categories = response
    $scope.getSubcategories = ->
      if !$scope.article.category.slug?
        $scope.subcategories = []
        return
      Restangular.all('subcategories').getList({category: $scope.article.category.slug}).then (response)->
        $scope.subcategories = response
    $scope.updateArticle = ->
      $scope.loading = true
      textTransformed = wizMarkdownSvc.Transform($scope.article.content_markdown)
      $scope.article.content = textTransformed
      $scope.article.put().then (response) ->
        $scope.loading = false
        $state.go 'admin.site.articles'
        ngNotify.set('¡Se guardó correctamente!', 'success')
      .catch (response) ->
        $scope.loadingContinue = false
        ngNotify.set('¡No se pudo guardar, intente de nuevo!', 'error')