angular.module 'tich'
  .controller 'AdminController', ($scope, $auth, $state, $mdSidenav) ->
    'ngInject'
    $scope.logout = ->
      if $mdSidenav('left').isOpen()
        $mdSidenav('left').close()
      $auth.signOut().then ->
        $state.go 'home.articles'
        false
      false

    $scope.toggleAdminPanel = ->
      $mdSidenav('admin').toggle()
      false
    false