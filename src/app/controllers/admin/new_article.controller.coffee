angular.module 'tich'
  .controller 'NewArticleController', ($scope, $rootScope, $state, Restangular, wizMarkdownSvc, ngNotify, $mdDialog) ->
    'ngInject'
    $scope.article = {}
    $scope.subcategories = []
    Restangular.all('categories').getList().then (response)->
      $scope.categories = response
    $scope.getSubcategories = ->
      if !$scope.article.category.slug?
        $scope.subcategories = []
        return
      Restangular.all('subcategories').getList({category: $scope.article.category.slug}).then (response)->
        $scope.subcategories = response
    $scope.cancel = ->
      $mdDialog.cancel()
    $scope.saveArticle = ->
      $scope.loading = true
      Restangular.all('admin').all('articles').post($scope.article).then (response) ->
        $scope.loading = false
        $scope.articles.unshift(response)
        $mdDialog.hide()
        $state.go 'admin.site.article.general', slug: response.slug
        ngNotify.set('¡Se guardó correctamente!', 'success')
      .catch (response) ->
        $scope.loading = false
        ngNotify.set('¡No se pudo guardar, intente de nuevo!', 'error')