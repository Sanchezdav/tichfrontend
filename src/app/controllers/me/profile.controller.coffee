angular.module 'tich'
  .controller 'ProfileController', ($scope, $rootScope, $auth, Restangular, ngNotify, Upload, apiUrl) ->
    'ngInject'
    $rootScope.section =
      title:"Perfil"
    $scope.url = apiUrl
    $scope.updatePassword = ->
      $auth.updatePassword $scope.profile
      .success (resp) ->
        ngNotify.set('¡Se guardó correctamente!', 'success')
    $scope.updateEmail =  ->
      $scope.loadingEmail = true
      $auth.updateAccount $scope.user
      .then ->
        $scope.loadingEmail = false
        ngNotify.set('¡Se guardó correctamente!', 'success')
    $scope.saveProfile =  ->
      $scope.loading = true
      $scope.me.put().then ->
        $scope.loading = false
        ngNotify.set('¡Se guardó correctamente!', 'success')
      .catch (error) ->
        $scope.loading = false
        ngNotify.set('¡No se pudo guardar!', 'error')
        $scope.error_alias = error.data[0]
    $scope.upload = (file)->
      $scope.loadingAvatar = true
      Upload.upload
        url: $scope.me.getRequestedUrl()
        file: file
        method: 'PUT'
      .success (data, status, headers, config)->
        $scope.user.avatar.sidenav.url = data.avatar.avatar.sidenav.url
        $scope.loadingAvatar = false
        ngNotify.set('¡Se guardó correctamente!', 'success')
        $scope.my_avatar = null
      .error (data, status, headers, config) ->
        $scope.loadingAvatar = false
        ngNotify.set('¡Ocurrió un error!', 'error')
    $scope.uploadCancel = ->
      $scope.my_avatar = null