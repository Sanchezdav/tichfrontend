angular.module 'tich'
  .controller 'ArticleController', ($scope, $stateParams, Restangular, apiUrl) ->
    'ngInject'
    $scope.url = apiUrl
    article_slug = $stateParams.slug
    Restangular.one("articles", article_slug).get().then (response) ->
      $scope.article = response
      if $scope.article
        $scope.article.count_views = $scope.article.count_views + 1
        $scope.article.put()
        $scope.article.all("discussions").getList().then (response) ->
          $scope.article.discussions = response
    $scope.saveDiscussion = (discussion) ->
      $scope.loadingDiscussion = true
      $scope.article.all('discussions').post(discussion).then (response) ->
        $scope.loadingDiscussion = false
        discussion.message = null
        $scope.article.discussions.unshift(response)
        if response.discussionable_type == "Article" && response.discussionable_id == $scope.article.id
          $scope.article.comments_count = response.discussionable.comments_count
    $scope.showComment = (discussion) ->
      if discussion.show
        discussion.show = false
      else
        discussion.show = true
    $scope.showCommentList = (discussion) ->
      if discussion.showComments
        discussion.showComments = false
      else
        discussion.showComments = true
    $scope.saveComment = (discussion) ->
      $scope.loadingComment = true
      discussion.all('comments').post(discussion.comment).then (response) ->
        $scope.loadingComment = false
        discussion.comment = null
        discussion.comments.unshift(response)