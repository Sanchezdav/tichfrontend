angular.module 'tich'
  .controller 'ArticlesController', ($scope, Restangular, apiUrl) ->
    'ngInject'
    $scope.url = apiUrl
    Restangular.all("articles").getList().then (response) ->
      $scope.articles = response