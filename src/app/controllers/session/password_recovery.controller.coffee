angular.module 'tich'
  .controller 'PasswordRecoveryController', ($scope, $auth, ngNotify) ->
    'ngInject'
    $scope.recoveryPassword = ->
      $scope.updateLoading = true
      $auth.requestPasswordReset $scope.recovery
      .then ->
        $scope.updateLoading = false
        ngNotify.set('¡Se ha enviado un correo electrónico!', 'success')()
      .catch ->
        $scope.updateLoading = false
        ngNotify.set('¡No se pudo enviar el correo electrónico!', 'error')()