angular.module 'tich'
  .controller 'EmailController', ($scope, $auth, $state, $timeout, ngNotify) ->
    'ngInject'
    $scope.updateEmail = ->
      $auth.updateAccount($scope.updateAccountForm)
      .then ->
        ngNotify.set('¡Se guardó correctamente!', 'success')
        $timeout (->
          $state.go "home.articles"
          )
          ,1e3
      .catch (res)->
        ngNotify.set('¡No se pudo guardar, intente de nuevo!', 'error')
    false



