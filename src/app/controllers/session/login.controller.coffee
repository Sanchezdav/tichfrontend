angular.module 'tich'
  .controller 'LoginController', ($scope, $auth, $location, $state) ->
    'ngInject'
    $scope.login = {}
    if $location.search().auth_token
      $state.go "home.articles"

    $scope.loadingCircle = ->
      $scope.loading = true

    $scope.loginSubmit = ->
      $scope.loginLoading = true
      $auth.submitLogin $scope.login
      .then (resp)->
        $scope.loginLoading = false
      .catch (resp) ->
        $scope.loginLoading = false
        $scope.errors = resp.errors
