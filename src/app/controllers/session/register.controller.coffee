angular.module 'tich'
  .controller 'RegisterController', ($scope, $auth, $location, $state, ngNotify) ->
    'ngInject'
    $scope.register = {}
    if $location.search().auth_token
      $state.go "home.articles"

    $scope.loadingCircle = ->
      $scope.loading = true

    $scope.registrationSubmit= ->
      $scope.loginLoading = true
      $auth.submitRegistration $scope.register
      .then (resp)->
        $scope.loginLoading = false
        ngNotify.set('¡Se te envió un correo, por favor confirma tu email!', 'success')
        $state.go "home.confirmation"
      .catch (resp) ->
        $scope.loginLoading = false
        ngNotify.set('¡No se pudo enviar el correo, por favor intenta de nuevo!', 'error')