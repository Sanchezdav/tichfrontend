angular
  .module 'tich', [
    'ngAnimate',
    'ngCookies',
    'ngSanitize',
    'ngMessages',
    'ngAria',
    'restangular',
    'ui.router',
    'ngMaterial',
    'toastr',
    'ng-token-auth',
    'ngMdIcons',
    'angular-ladda',
    'ngNotify',
    'ngFileUpload',
    'angular-loading-bar',
    'md.data.table',
    'simplePagination',
    'angularTrix',
    'wiz.markdown',
    'mgo-angular-wizard',
    'angularMoment'
  ]
  .config (RestangularProvider, apiUrl) ->
    RestangularProvider.setBaseUrl(apiUrl)
  .config ($mdThemingProvider) ->
    $mdThemingProvider.theme('default')
      .primaryPalette('green')
      .accentPalette('blue')
      .warnPalette('red')
  .config (laddaProvider) ->
    laddaProvider.setOption
      style: 'expand-right',
  .config ($authProvider, apiUrl)->
    $authProvider.configure
      apiUrl: apiUrl
      storage: 'localStorage'
      omniauthWindowType: 'sameWindow'
      authProviderPaths:
        twitter:   '/auth/twitter'
        facebook: '/auth/facebook'
        google:   '/auth/google_oauth2'
  .config (cfpLoadingBarProvider)->
    cfpLoadingBarProvider.includeSpinner = false
  .run (amMoment) ->
    amMoment.changeLocale('es')
  .run ($rootScope,$auth,$state, $stateParams, ngNotify) ->
    ngNotify.config
      theme: 'pure',
      position: 'top',
      duration: 3000,
      type: 'success',
      sticky: false,
      button: true,
      html: false
    $rootScope.$on 'auth:login-success', (ev, user) ->
      $state.go "home.articles"
    $rootScope.$on 'auth:registration-email-success', (ev, user) ->
      $state.go "home.login"
    $rootScope.$on 'auth:logout-success', (ev, user) ->
      $state.go "home.articles"
    $rootScope.$on 'auth:login-error', (ev, reason) ->
      console.log reason
      $state.go "home.login"
    $rootScope.$on 'auth:registration-email-error', (ev, reason) ->
      console.log reason
      $state.go "home.register"
    $rootScope.$on 'auth:email-confirmation-success', (ev, user, $state) ->
      $state.go "home.articles"
    $rootScope.$on 'auth:password-reset-request-success', (ev, data) ->
      $state.go "home.login"