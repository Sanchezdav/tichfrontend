## README

# Tich #

Sitio web que comparte artículos sobre informática y tecnología.

## Tecnologías

Creado con generator-gulp-angular, utilizando Angular 1.5 y angular material para el diseño y grid, coffescript, jade y stylus.

## Instalación

Para poder correr el proyecto es necesario instalar yo, gulp, bower y generator-gulp-angular para así tener todas las librerías completas y conectarlo con el servidor y obtener las apis de TichApi.

## Colaboradores

### Fausto David Sánchez Salazar
### Ing. en Informática
Twitter: @SanchezDav90